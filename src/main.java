
import interfaz.Menu;
import interfaz.Sortear;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import javax.swing.JFrame;
import logica.Jugador;
import logica.Lista;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Samer
 */
public class main {
    public static void main(String args[]){
        try{
            Class.forName("com.mysql.jdbc.Driver");
        }catch(ClassNotFoundException e){
            System.out.println("No se encontro el driver");
            System.exit(0);
        }
        
        Connection conn = null;
    
        try {
            conn = DriverManager.getConnection("jdbc:mysql://localhost/mundial?user=root");
                Statement stmt  = conn.createStatement();

                //Pide los estadios a la db y los guarda en Lista.estadios
                ResultSet rs = stmt.executeQuery("SELECT * FROM estadios");                
                while (rs.next()){
                    Lista.addEstadio(rs.getString("nombre"),rs.getString("ciudad"));
                }
                //Pide los clubes a la db y los guarda en Lista.clubes
                rs = stmt.executeQuery("SELECT * FROM clubes");                
                while (rs.next()){
                    Lista.addClub(rs.getInt("id"),rs.getString("nombre"),rs.getString("pais"));
                }
                //Pide las selecciones a la db y los guarda en Lista.selecciones
                rs = stmt.executeQuery("SELECT * FROM selecciones");                
                while (rs.next()){
                    Lista.addSeleccion(rs.getString("nombre"),rs.getString("confederacion"),rs.getString("grupo").charAt(0),rs.getInt("pos_grupo"),rs.getInt("pos_fifa"));
                }
                //Pide las jugadores a la db y los guarda en Lista.jugadores
                rs = stmt.executeQuery("SELECT * FROM jugadores");                
                while (rs.next()){
                    
                    // Separa los datos de la fecha por '-'
                    String[] fechaNac =  rs.getString("fnac").split("-");
                    String[] fechaDebut =  rs.getString("fdebut").split("-");
                    // Convierte los datos de la fecha de forma legible para date
                    Date fnac = new Date(fechaNac[0]+'/'+fechaNac[1]+'/'+fechaNac[2]);
                    Date fdebut = new Date(fechaDebut[0]+'/'+fechaDebut[1]+'/'+fechaDebut[2]);
                    
                    //Agrega al jugador
                    Lista.addJugador(rs.getInt("ci"),rs.getString("nombre"),rs.getString("apellido"),rs.getInt("numero_camiseta"),rs.getString("nombre_selec"),rs.getInt("id_club"),fnac,rs.getInt("altura"),rs.getInt("peso"),fdebut);
                }
                //Pide las posiciones a la db y los guarda en Lista.posiciones
                rs = stmt.executeQuery("SELECT * FROM posiciones");                
                while (rs.next()){
                    Lista.addPosicion(rs.getInt("ci"),rs.getString("nombre_selec"),rs.getString("posicion"));
                }
                //Pide los partidos a la db y los guarda en Lista.partidos
                rs = stmt.executeQuery("SELECT * FROM partidos");                
                while (rs.next()){
                    String[] fechaNac =  rs.getString("fecha").split("-");
                    String hora =  rs.getString("hora");
                    // Convierte los datos de la fecha de forma legible para date
                    Date fecha = new Date(fechaNac[0]+'/'+fechaNac[1]+'/'+fechaNac[2]+' '+hora);
                    
                    //Agreaga los partidos con los date
                    Lista.addPartido(rs.getString("selec1"), rs.getString("selec2"), fecha, rs.getString("fase"), rs.getString("nombre_estadio"));
                }
                //Pide los Goles a la db y los guarda en Lista.goles
                rs = stmt.executeQuery("SELECT * FROM goles");                
                while (rs.next()){
                    String[] fechaNac =  rs.getString("fecha").split("-");
                    // Convierte los datos de la fecha de forma legible para date
                    Date fecha = new Date(fechaNac[0]+'/'+fechaNac[1]+'/'+fechaNac[2]);
                    
                    //Agreaga los partidos con los date
                    Lista.addGol(rs.getInt("ci"),rs.getString("selec1"), rs.getString("selec2"), fecha, rs.getInt("minuto"), rs.getString("tiempo"),rs.getInt("valor"));
                }
                
                System.out.println("Estadios: "+Lista.getEstadios().size());
                System.out.println("Clubes: "+Lista.getClubes().size());
                System.out.println("Selecciones: "+Lista.getSelecciones().size());
                System.out.println("Jugadores: "+Lista.getJugadores().size()+" en "+Lista.getPosiciones().size()+" posiciones");
                System.out.println("Partidos: "+Lista.getPartidos().size());
                System.out.println("Goles: "+Lista.getGoles().size());
                Menu.frame.setVisible(true);
            // Do something with the Connection
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
            System.exit(1);
        }
    }
}
