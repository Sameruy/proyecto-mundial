/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import java.sql.Time;
import java.util.Date;

/**
 *
 * @author Samer
 */
public class Gol {
    private int ci;
    private String selec1;
    private String selec2;
    private Date fecha;
    private int minuto;
    private String tiempo;
    private int valor;

    public Gol(int ci, String selec1, String selec2, Date fecha, int minuto, String tiempo, int valor) {
        this.ci = ci;
        this.selec1 = selec1;
        this.selec2 = selec2;
        this.fecha = fecha;
        this.minuto = minuto;
        this.tiempo = tiempo;
        this.valor = valor;
    }
    
}
