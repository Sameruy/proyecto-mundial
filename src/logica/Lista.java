/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Samer
 */
public class Lista {
    private static ArrayList<Seleccion> selecciones = new ArrayList<Seleccion>();
    private static ArrayList<Club> clubes = new ArrayList<Club>();
    private static ArrayList<Estadio> estadios = new ArrayList<Estadio>();
    private static ArrayList<Partido> partidos = new ArrayList<Partido>();
    private static ArrayList<Jugador> jugadores = new ArrayList<Jugador>();
    private static ArrayList<Posicion> pocisiones = new ArrayList<Posicion>();
    private static ArrayList<Gol> goles = new ArrayList<Gol>();

    public static ArrayList<Seleccion> getSelecciones() {
        return selecciones;
    }

    public static void addSeleccion(String nombre,String confederacion,char grupo,int pos_grupo,int pos_fifa) {
        Lista.selecciones.add(new Seleccion(nombre,confederacion,grupo,pos_grupo,pos_fifa));
    }

    public static ArrayList<Club> getClubes() {
        return clubes;
    }

    public static void addClub(int id,String nombre,String pais) {
        Lista.clubes.add(new Club(id,nombre,pais));
    }

    public static ArrayList<Estadio> getEstadios() {
        return estadios;
    }

    public static void addEstadio(String nombre,String ciudad) {
        Lista.estadios.add(new Estadio(nombre,ciudad));
    }

    public static ArrayList<Partido> getPartidos() {
        return partidos;
    }

    public static void addPartido(String selec1,String selec2,Date fecha,String fase,String estadio) {
        Lista.partidos.add(new Partido(selec1,selec2,fecha,fase,estadio));
    }

    public static ArrayList<Jugador> getJugadores() {
        return jugadores;
    }

    public static void addJugador(int ci, String nombre, String apellido,int numero_camiseta,String nombre_selec,int club,Date fnac,int altura,int peso,Date fdebut) {
        Lista.jugadores.add(new Jugador(ci,nombre,apellido,numero_camiseta,nombre_selec,club,fnac,altura,peso,fdebut));
    }

    public static ArrayList<Posicion> getPosiciones() {
        return pocisiones;
    }

    public static void addPosicion(int ci,String nombre_selec,String posicion) {
        Lista.pocisiones.add(new Posicion(ci,nombre_selec,posicion));
    }

    public static ArrayList<Gol> getGoles() {
        return goles;
    }

    public static void addGol(int ci,String selec1,String selec2,Date fecha,int minuto,String tiempo,int valor) {
        Lista.goles.add(new Gol(ci,selec1,selec2,fecha,minuto,tiempo,valor));
    }

    public static void sortearGrupos(){
        ArrayList<Seleccion> manipular = new ArrayList<Seleccion>();
        Seleccion a = selecciones.get(0);
        Seleccion b = selecciones.get(1);
        
        //Ordena segun su pos en la fifa
        for (int i = 0; i < selecciones.size(); i++) {
            a = selecciones.get(i);
            for (int j = i+1; j < selecciones.size(); j++) {
                b = selecciones.get(j);
                if(a.getPos_fifa() > b.getPos_fifa()){
                    selecciones.set(i,b);
                    selecciones.set(j,a);
                    a = b;
                }
            }
                 
        }
        //los separa en grupo de 8
        for (int i = 0; i < selecciones.size(); i++) {
            selecciones.get(i).setPos_grupo((int) (i/8)+1);
        }
        //Les asigna un grupo alateoriamente
        char[] letras = Seleccion.getLetras();
        for (int i = 0; i < 8; i++) {
            int letra = (int) (Math.random() * letras.length-1) + 1;
            selecciones.get(i).setGrupo(letras[letra]);
        }
        
        
        
        System.out.println("Se sortearon los grupos");
        for (int i = 0; i < selecciones.size(); i++) {
            a = selecciones.get(i);
            System.out.println(a.getNombre()+" # "+a.getGrupo()+" # "+a.getPos_grupo()+" # "+a.getPos_fifa());
        }
        System.exit(0);
    }
    
}
