/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import java.util.Date;

/**
 *
 * @author Samer
 */
public class Jugador {
    private int ci;
    private String nombre;
    private String apellido;
    private int numero_camiseta;
    private String seleccion;
    private int club;
    private Date fnac;
    private int altura;
    private int peso;
    private Date fdebut;

    public Jugador(int ci, String nombre, String apellido, int numero_camiseta, String seleccion) {
        this.ci = ci;
        this.nombre = nombre;
        this.apellido = apellido;
        this.numero_camiseta = numero_camiseta;
        this.seleccion = seleccion;
    }

    public Jugador(int ci, String nombre, String apellido, int numero_camiseta, String seleccion, int club, Date fnac, int altura, int peso, Date fdebut) {
        this.ci = ci;
        this.nombre = nombre;
        this.apellido = apellido;
        this.numero_camiseta = numero_camiseta;
        this.seleccion = seleccion;
        this.club = club;
        this.fnac = fnac;
        this.altura = altura;
        this.peso = peso;
        this.fdebut = fdebut;
    }

    
    
    public int getCi() {
        return ci;
    }


    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public int getNumero_camiseta() {
        return numero_camiseta;
    }

    public void setNumero_camiseta(int numero_camiseta) {
        this.numero_camiseta = numero_camiseta;
    }

    public String getSeleccion() {
        return seleccion;
    }

    public void setSeleccion(String seleccion) {
        this.seleccion = seleccion;
    }

    public int getClub() {
        return club;
    }

    public void setClub(int club) {
        this.club = club;
    }

    public Date getFnac() {
        return fnac;
    }

    public void setFnac(Date fnac) {
        this.fnac = fnac;
    }

    public int getAltura() {
        return altura;
    }

    public void setAltura(int altura) {
        this.altura = altura;
    }

    public int getPeso() {
        return peso;
    }

    public void setPeso(int peso) {
        this.peso = peso;
    }

    public Date getFdebut() {
        return fdebut;
    }

    public void setFdebut(Date fdebut) {
        this.fdebut = fdebut;
    }
    
    
    
    
    
}
