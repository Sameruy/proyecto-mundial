/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import java.sql.Time;
import java.util.Date;

/**
 *
 * @author Samer
 */
public class Partido {
    private String selec1;
    private String selec2;
    private Date fecha;
    private String fase;
    private String estadio;

    public Partido(String selec1, String selec2, Date fecha, String fase, String estadio) {
        this.selec1 = selec1;
        this.selec2 = selec2;
        this.fecha = fecha;
        this.fase = fase;
        this.estadio = estadio;
    }

    public String getSelec1() {
        return selec1;
    }

    public void setSelec1(String selec1) {
        this.selec1 = selec1;
    }

    public String getSelec2() {
        return selec2;
    }

    public void setSelec2(String selec2) {
        this.selec2 = selec2;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }


    public String getFase() {
        return fase;
    }

    public void setFase(String fase) {
        this.fase = fase;
    }

    public String getEstadio() {
        return estadio;
    }

    public void setEstadio(String estadio) {
        this.estadio = estadio;
    }
    
    
}
