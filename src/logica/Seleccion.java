package logica;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Samer
 */
public class Seleccion {
    private String nombre;
    private String confederacion;
    private char grupo;
    private int pos_grupo;
    private int pos_fifa;
    private static char[] letrasM = {'A','B','C','D','E','F','G','H'};

    public Seleccion(String nombre, String confederacion, char grupo, int pos_grupo, int pos_fifa) {
        this.nombre = nombre;
        this.confederacion = confederacion;
        this.grupo = grupo;
        this.pos_grupo = pos_grupo;
        this.pos_fifa = pos_fifa;
    }

    public Seleccion(String nombre, String confederacion, int pos_fifa) {
        this.nombre = nombre;
        this.confederacion = confederacion;
        this.pos_fifa = pos_fifa;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getConfederacion() {
        return confederacion;
    }

    public void setConfederacion(String confederacion) {
        this.confederacion = confederacion;
    }

    public char getGrupo() {
        return grupo;
    }

    public void setGrupo(char grupo) {
        this.grupo = grupo;
    }

    public int getPos_grupo() {
        return pos_grupo;
    }

    public void setPos_grupo(int pos_grupo) {
        this.pos_grupo = pos_grupo;
    }

    public int getPos_fifa() {
        return pos_fifa;
    }

    public void setPos_fifa(int pos_fifa) {
        this.pos_fifa = pos_fifa;
    }
    public static char[] getLetras(){
        return letrasM;
    }
    
}
